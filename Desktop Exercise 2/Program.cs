﻿using System;

namespace Desktop_Exercise_2
{
  class Program
  {
    static void Main(string[] args)
    {
      var arr = ArrayFactory.GetArray();
      
      Console.WriteLine("**** OutputArray ****");
      ArrayFactory.OutputArray();
      
      Console.WriteLine("\r\n**** AverageArrayValue ****");
      ArrayFactory.AverageArrayValue();

      Console.WriteLine("\r\n**** MinArrayValue ****");
      ArrayFactory.MinArrayValue();

      Console.WriteLine("\r\n**** MaxArrayValue ****");
      ArrayFactory.MaxArrayValue();

      Console.WriteLine("\r\n**** SortArrayAsc ****");
      ArrayFactory.SortArrayAsc();
      
      Console.Read();
    }
  }
}
